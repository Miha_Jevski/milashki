<div class="sidebar">
    <ul class="nav nav-tabs">
        <li>
           <a href="<?php echo $home; ?>"><?php echo $text_home; ?></a>
        </li>
        <?php foreach ($informations as $information) { ?>
            <li>
                <a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
            </li>
        <?php } ?>
        <li>
            <a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a>
        </li>

        <!-- <li>
            <a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a>
        </li> -->
    </ul>
</div>
<!--  nav-stacked -->