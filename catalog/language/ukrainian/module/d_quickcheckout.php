<?php 

$_['title_payment_address']  						= 'Контактні дані';
$_['description_payment_address']  					= '';
$_['title_shipping_address']  						= 'Деталі доставки';
$_['description_shipping_address'] 					= '';
$_['title_shipping_method']  						= 'Спосіб доставки';
$_['description_shipping_method'] 					= '';
$_['title_payment_method'] 							= 'Спосіб оплати';
$_['description_payment_method'] 					= '';
$_['title_shopping_cart'] 							= 'Корзина';
$_['description_shopping_сart'] 					= '';

$_['error_field_required'] 							= "Це поле обов'язкове";

$_['entry_email_confirm'] 							= 'Підтвердіть E-mail';
$_['error_email_confirm'] 							= 'E-mail не співпадає!';


$_['step_option_guest_desciption'] = 'Use guest checkout to buy without creating an account';

$_['error_step_payment_address_fields_company'] = 'Company name required more the 3 and less the 128 characters';
$_['error_step_shipping_address_fields_company'] = 'Company name required more the 3 and less the 128 characters';

$_['error_step_confirm_fields_comment'] = 'Please fill in the comment to the order';


?>