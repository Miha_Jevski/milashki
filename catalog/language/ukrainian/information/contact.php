<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['heading_title']  = 'Контакти';

// Text
$_['text_location']  = 'Наш адрес';
$_['text_store']     = 'Наш магазин';
$_['text_contact']   = 'Форма зв`язку';
$_['text_address']   = 'Адреса';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Графік роботи';
$_['text_comment']   = 'Коментар';
$_['text_success']   = '<p>Ваше повідомлення було успішно відправлено до адміністрації магазину!</p>';

// Entry
$_['entry_name']     = 'Ваше ім`я';
$_['entry_email']    = 'E-Mail';
$_['entry_enquiry']  = 'Повідомлення';
$_['entry_captcha']  = 'Введіь код в полі нижче';

// Email
$_['email_subject']  = 'Повідомлення %s';

// Errors
$_['error_name']     = 'Ім`я повинно містити від 3 до 32 символів!';
$_['error_email']    = 'Неправильний E-Mail!';
$_['error_enquiry']  = 'Повідомлення повинно містити від 10 до 3000 символів!';
$_['error_captcha']  = 'Код підтвердження не відповідає зображенню!';