<?php

// Text

$_['text_items']    = '%s товар(ів) - %s';
$_['text_empty']    = 'Ваш кошик порожній!';
$_['text_cart']     = 'Редагувати Кошик';
$_['text_checkout'] = 'Оформити замовлення';
$_['text_recurring']  = 'Профіль оплати';