<?php
// Heading
$_['heading_title']    = 'Gunsel';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified delivery from store!';
$_['text_edit']        = 'Edit delivery From Store Shipping';

// Entry
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify delivery from store!';