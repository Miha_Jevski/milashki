<?php
// Heading
$_['heading_title']    = 'Делівері';

// Text
$_['text_shipping']    = 'Доставка';
$_['text_success']     = 'Ви успішно змінили налаштування!';
$_['text_edit']        = 'Редагувати';

// Entry
$_['entry_geo_zone']   = 'Географічна зона';
$_['entry_status']     = 'Статус';
$_['entry_sort_order'] = 'Порядок сортування';

// Error
$_['error_permission'] = 'У Вас немає доступу до змін цього модуля!';

