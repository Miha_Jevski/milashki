<?php
// Heading
$_['heading_title']      = 'Приват 24';

// Text 
$_['text_payment']       = 'Оплата';
$_['text_success']       = 'Настройки модуля обновлены!';   
$_['text_pay']           = 'Приват 24';
$_['text_card']          = 'Credit Card';
$_['text_edit']          = 'Изменить';

// Entry
$_['entry_merchant']     = 'ID мерчанта';
$_['entry_signature']    = 'Пароль';
$_['entry_type']         = 'Тестовый режим';
$_['entry_total']        = 'Минимальная сумма заказа';
$_['entry_order_status'] = 'Статус заказа после оплаты';
$_['entry_geo_zone']     = 'Географическая зона';
$_['entry_status']       = 'Статус';
$_['entry_sort_order']   = 'Порядок сортировки';

$_['help_total']   			 = 'Ниже этой суммы метод будет недоступен.';

// Error
$_['error_permission']   = 'У Вас нет прав для управления этим модулем!';
$_['error_merchant']     = 'Неверный ID магазина!';
$_['error_signature']    = 'Отсутствует пароль!';
?>