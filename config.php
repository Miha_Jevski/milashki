<?php
// HTTP
define('HTTP_SERVER', 'http://milashki/');

// HTTPS
define('HTTPS_SERVER', 'http://milashki/');

// DIR
define('DIR_APPLICATION', '/home/mykhaylo/develop/study/own/milashki/catalog/');
define('DIR_SYSTEM', '/home/mykhaylo/develop/study/own/milashki/system/');
define('DIR_LANGUAGE', '/home/mykhaylo/develop/study/own/milashki/catalog/language/');
define('DIR_TEMPLATE', '/home/mykhaylo/develop/study/own/milashki/catalog/view/theme/');
define('DIR_CONFIG', '/home/mykhaylo/develop/study/own/milashki/system/config/');
define('DIR_IMAGE', '/home/mykhaylo/develop/study/own/milashki/image/');
define('DIR_CACHE', '/home/mykhaylo/develop/study/own/milashki/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/mykhaylo/develop/study/own/milashki/system/storage/download/');
define('DIR_LOGS', '/home/mykhaylo/develop/study/own/milashki/system/storage/logs/');
define('DIR_MODIFICATION', '/home/mykhaylo/develop/study/own/milashki/system/storage/modification/');
define('DIR_UPLOAD', '/home/mykhaylo/develop/study/own/milashki/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'milashki');
define('DB_PASSWORD', 'milashki');
define('DB_DATABASE', 'milashki');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
